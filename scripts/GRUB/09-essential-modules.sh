#!/bin/bash

cat >> $target/etc/initramfs-tools/modules <<EOF
qcom_spmi_pmic
qcom_rpm
hi655x_pmic
EOF
for kernel in $target/boot/vmlinuz-*
do
    chroot $target /usr/sbin/update-initramfs -c -k $ver||true
done
